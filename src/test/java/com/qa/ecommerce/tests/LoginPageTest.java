package com.qa.ecommerce.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.qa.ecommerce.utils.ConstantUtil;

public class LoginPageTest extends BaseTest {

@Test(priority =1)
 public void getloginpagetiletest() 
 {

 String ActualTitle = loginpage.getloginpagetile(); 
 Assert.assertEquals(ActualTitle, ConstantUtil.LOGIN_PAGE_TITLE);
	
}

@Test(priority =2)
public void getcurrenturltest () 
{
	
 String Actaulurl = loginpage.getcurrenturl();
 Assert.assertEquals(Actaulurl, ConstantUtil.OPEN_CART_URL );
}

@Test(priority =3)
public void isforgotpaswordlinkexisttest ()
{
 Assert.assertTrue(loginpage.isforgotpaswordlinkexist());
}

@Test(priority =4)

public void dologintest ()
{
	loginpage.dologin();
}
}
