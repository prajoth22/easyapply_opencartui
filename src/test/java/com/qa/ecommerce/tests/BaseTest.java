package com.qa.ecommerce.tests;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import com.qa.ecommerce.factory.Driverfactory;
import com.qa.ecommerce.pages.LoginPage;

public class BaseTest {
	
	Driverfactory Df;
	WebDriver driver;
	Properties prop;
	LoginPage loginpage;

		@BeforeTest
		public void setup()
		{
		 Df = new Driverfactory();
		 prop = Df.init_prop();
		 driver= Df.setup(prop);
		 loginpage = new LoginPage(driver);
		}
		
		
		
		@AfterTest 
		public void teardown() 
		{
		 driver.close();
		}
		
		

	}

