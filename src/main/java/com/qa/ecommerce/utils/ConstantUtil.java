package com.qa.ecommerce.utils;

public class ConstantUtil {

	public static final String LOGIN_PAGE_TITLE = "OpenCart - Account Login";

	public static final String OPEN_CART_URL = "https://www.opencart.com/index.php?route=account/login";
	
}
