package com.qa.ecommerce.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

// 1. Declare private driver 
	
	private WebDriver driver;
	
//2. Page Constructor
	
	public LoginPage(WebDriver driver) 
	{
	 	this.driver = driver;
	}
	
//3. By locators
  
    private By email = By.id("input-email");
    private By password = By.id("input-password");
    private By Loginbtn = By.xpath("//*[@id=\"account-login\"]/div[2]/div/div[1]/form/div[3]/div[1]/button[1]");
    private By forgotpasswordlink = By.xpath("//*[@id=\"account-login\"]/div[2]/div/div[1]/form/div[3]/div[2]/a");
	
//4. Page Actions 
	
    
    public String getloginpagetile()
    {
     return driver.getTitle();
    }
    
    
    public String getcurrenturl()
    {
     return driver.getCurrentUrl();
    }
    
    public boolean isforgotpaswordlinkexist () 
    {
     return driver.findElement(forgotpasswordlink).isDisplayed();
    }
    
    public void dologin () 
    {
     driver.findElement(email).sendKeys("Prajothba@gmail.com");
     driver.findElement(password).sendKeys("Demo@2022");
     driver.findElement(Loginbtn).click();
    }
    
}
