package com.qa.ecommerce.factory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

/*
 This is used to initialize the browser 
 @return = driver 
*/


public class Driverfactory {
	
	public WebDriver driver;
	public Properties prop;
	
	public WebDriver setup (Properties prop)
   {
    String BrowserName =prop.getProperty("Browser");
	System.out.println("Browser Name is: "+BrowserName );
		
	 if (BrowserName.equals("Chrome")) 
	 {
		 WebDriverManager.chromedriver().setup();
		 driver = new ChromeDriver();
	 }
	 else if (BrowserName.equals("Edge")) 
	 {
		 WebDriverManager.edgedriver().setup();
		 driver = new EdgeDriver();
	 }
	 else if (BrowserName.equals("Firefox"))
	 {
		 WebDriverManager.firefoxdriver().setup();
		 driver = new FirefoxDriver();
	 }
	 else
	 {
		System.out.println("Browser not supported or Please input your browser name correctly"); 
		 
	 }
	 
	 driver.manage().window().fullscreen();
	 driver.manage().deleteAllCookies();
	 driver.get(prop.getProperty("URL"));
	 return driver;
	}
	
	/*
	 * To initialize the properties in the config file 
	*/
	
	public Properties init_prop() 
{
	prop = new Properties();
	try {
		FileInputStream ip = new FileInputStream ("./src/test/resources/Config/config.properties");
		try {
			prop.load(ip);
		} catch (IOException e) {
			e.printStackTrace();
		}
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}
	return prop;
     }
}